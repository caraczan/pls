/**
 * repository:  https://gitlab.com/caraczan/pls/
 * author:      caraczan
 * owner:       caraczan
 */

// general includes
#include <algorithm>
#include <filesystem>
#include <iostream>
#include <ostream>
#include <string>
#include <sstream>

// private includes
#include "pls_options.hpp"
#include "pls_list.hpp"
#include "pls_print.hpp"


int main(int argc, char* argv[]) {
    // Arguments container class
    Arguments args {};

    // Handling options with wrapper around boost lib
    Options opts = Options(argc, argv, &args);
    opts.parse_program_config();
    if(!opts.parse())
        return 1;

    // Creating already curated list that will be passed to printing
    List entries_list = List(&args);
    entries_list.filter();
    entries_list.calcualate_col_widths();
    entries_list.sort();

    // Listing entries in specified place
    Print printer = Print(&entries_list, &args);
    printer.print();

    return 0;
}
