//
// Created by karaczan on 25.01.23.
//

#ifndef PLS_PLS_PRINT_HPP
#define PLS_PLS_PRINT_HPP

// Includes
#include <iomanip>
#include <map>
#include <chrono>

// Internal Includes#include "pls_entry.hpp"
#include "pls_list.hpp"
#include "pls_options.hpp"


// Custom definitions
// the following are UBUNTU/LINUX, and MacOS ONLY terminal color codes.
// POSSIBLE FIX change to constexpr or existing library
static struct colors {
    // defined general colors
    std::string reset       {"\033[0m"};
    std::string black       {"\033[30m"};   /* Black */
    std::string red         {"\033[31m"};   /* Red */
    std::string green       {"\033[32m"};   /* Green */
    std::string yellow      {"\033[33m"};   /* Yellow */
    std::string blue        {"\033[34m"};   /* Blue */
    std::string magenta     {"\033[35m"};   /* Magenta */
    std::string cyan        {"\033[36m"};   /* Cyan */
    std::string white       {"\033[37m"};   /* White */
    std::string boldblack   {"\033[1m\033[30m"};    /* Bold Black */
    std::string boldred     {"\033[1m\033[31m"};    /* Bold Red */
    std::string boldgreen   {"\033[1m\033[32m"};    /* Bold Green */
    std::string boldyellow  {"\033[1m\033[33m"};    /* Bold Yellow */
    std::string boldblue    {"\033[1m\033[34m"};    /* Bold Blue */
    std::string boldmagenta {"\033[1m\033[35m"};    /* Bold Magenta */
    std::string boldcyan    {"\033[1m\033[36m"};    /* Bold Cyan */
    std::string boldwhite   {"\033[1m\033[37m"};    /* Bold White */

    // definitions that allows for simply change of colors here instead of somewhere in code
    std::string cl_regular_file {reset};
    std::string cl_exe_file     {green};
    std::string cl_dir          {boldgreen};
    std::string cl_symlink      {cyan};
    std::string cl_special      {boldyellow};
    std::string cl_fifo         {yellow};
    std::string cl_socket       {boldmagenta};
    std::string cl_not_exist    {boldred};
} colors;

// Classes
/**
 * Class with singular purpose of printing correctly colored and formatted List.
 */
class Print {
private:
    // === VARIABLES ===
    List *entries_list;
    Arguments *args;

    // === FUNCTIONS ===
    static std::string_view choose_color(const Entry &entry);

public:
    // === CONSTRUCTORS ===
    explicit Print(List *_entries_list, Arguments *_args) :
        entries_list(_entries_list), args(_args)
    {};

    // === FUNCTIONS ===
    void print();
    void print_std();
    void print_list();
};

#endif