//
// Created by karaczan on 16.01.23.
//
#include "pls_entry.hpp"
#include <filesystem>
#include <iostream>
#include <ostream>

static std::string time_point_to_string(const fs::file_time_type &tp, const std::string &format, bool withMs = true, bool utc = true) {
    const fs::file_time_type::time_point::duration tt = tp.time_since_epoch();
    const time_t durS = std::chrono::duration_cast<std::chrono::seconds>(tt).count();
    std::ostringstream ss;
    if (const std::tm *tm = (utc ? std::gmtime(&durS) : std::localtime(&durS))) {
        ss << std::put_time(tm, format.c_str());
        if (withMs) {
            const long long durMs = std::chrono::duration_cast<std::chrono::milliseconds>(tt).count();
            ss << std::setw(3) << std::setfill('0') << int(durMs - durS * 1000);
        }
    }
    // gmtime/localtime() returned null ?
    else {
        ss << "<FORMAT ERROR>";
    }
    return ss.str();
}

// ---=== public ===---
/**
 * Constructor for class Entry
 * @param fs_entry - std::filesystem::directory_entry&
 */
Entry::Entry(const fs::directory_entry& fs_entry) {
    // adding permissions string representation
    save_permissions(fs::status(fs_entry).permissions());

    // assigns correct type to enum and type specifier for printings
    assign_type(fs_entry);
    fs_is_symlink = fs_entry.is_symlink();

    // assigns name of file to instance variable
    fs_name = {fs_entry.path().filename().string()};

    // assigns true or false if file is executable
    fs_is_executable = fs_permissions_s.find('x') != std::string::npos;

    // checks whether file is regular or symlink and assigns file size
    // otherwise nothing as we can't determine size of other special file types
    if (fs_type == fs_typer::is_regular_file or fs_type == fs_typer::is_symlink) {
        fs_size_byte = fs_entry.file_size();
    }
    fs_size_unit = "By";

    // Converting to proper unit.
    convert_to_unit(); 

    // Rounding number to 2 numbers & converting to string.
    std::ostringstream oss;
    oss << std::setprecision(3) << std::noshowpoint << fs_size;
    fs_size_s = oss.str();

    // Checking and assigning owner and group
    determine_user_and_gruop();
    
    // assign datetime to variable
    fs_last_write_time = fs::last_write_time(fs_entry);
    fs_last_write_time_s = time_point_to_string(fs_last_write_time, "%m-%d %H:%M:%S", false, true);
    // possible solution search from left side until first number is found
    // counting how many 10 positions are and assigning correct number

    // use function assign_type() to create type for symlink target
    if (fs_is_symlink) {
        fs_symlink_target_path = fs::read_symlink(fs_entry);
    }
}

bool Entry::operator < (const Entry& obj) const {
  return (fs_name < obj.fs_name);
};
bool Entry::operator > (const Entry& obj) const {
  return (fs_name > obj.fs_name);
};

// ---=== private ===---

/**
 * [Entry::round_up] round up value to number of decimal places
 * @param  value          Value to be rounded
 * @param  decimal_places Number of signicifent places after .
 * @return                Rounded value
 */
double Entry::round_up(double value, int decimal_places) {
    const double multiplier = std::pow(10.0, decimal_places);
    return std::ceil(value * multiplier) / multiplier;
}

/**
 * [Entry::determine_user_and_gruop] From libraries stat.h pwd.h grp.h determine owner, group and save it to entry values
 */
void Entry::determine_user_and_gruop() {
    // unfortunately this is the only way to find out about user and group
    // this only works on linux and unix
    struct stat info {};
    stat(fs_name.c_str(), &info);  // Error check omitted
    struct passwd *pw = getpwuid(info.st_uid);
    struct group  *gr = getgrgid(info.st_gid);
    fs_owner = pw->pw_name;
    fs_group = gr->gr_name;
}

/**
 * [Entry::convert_to_unit] Convert fs_size_byte to most optimal (max xxx.xx) unit/more human readable 
 */
void Entry::convert_to_unit() {
    std::string size_s = std::to_string(fs_size_byte);
    uint size_len = size_s.length();
    uint div = size_len / 3;
    uint rem = size_len % 3;

    if (rem != 0) {
        fs_size = std::stod(size_s.insert(rem, 1, '.'));
    } else {
        fs_size = std::stod(size_s.insert(3, 1, '.'));
        div--;
    }

    switch (div) {
        case 0:
            fs_size_unit = "By";
            break;
        case 1:
            fs_size_unit = "KB";
            break;
        case 2:
            fs_size_unit = "MB";
            break;
        case 3:
            fs_size_unit = "GB";
            break;
        case 4:
            fs_size_unit = "TB";
            break;
        case 5:
            fs_size_unit = "PB";
            break;
        default:
            fs_size_unit = "!!";
            break;
    }

    if (fs_size_byte == 0) {
        fs_size_unit = "!!";
    }
}

/**
 * Assigns correct type of file to instance enum Entry::fs_type from enum Entry::fs_typer
 * and correct string specifier (fs_type_s) for use in Print class.
 * @param fs_entry - const std::filesystem::directory_entry&
 */
void Entry::assign_type(const fs::directory_entry& fs_entry) {
    if (fs::is_directory(fs_entry)) {
        fs_type = Entry::fs_typer::is_directory;
        fs_type_s = 'd';
    } else if (fs::is_symlink(fs_entry)) {
        fs_type = Entry::fs_typer::is_symlink;
        fs_type_s = 'l';
    } else if (fs::is_block_file(fs_entry)) {
        fs_type = Entry::fs_typer::is_block_file;
        fs_type_s = 'b';
    } else if (fs::is_character_file(fs_entry)) {
        fs_type = Entry::fs_typer::is_character_file;
        fs_type_s = 'c';
    } else if (fs::is_fifo(fs_entry)) {
        fs_type = Entry::fs_typer::is_fifo;
        fs_type_s = 'p';
    } else if (fs::is_socket(fs_entry)) {
        fs_type = Entry::fs_typer::is_socket;
        fs_type_s = 's';
    } else {
        fs_type = Entry::fs_typer::is_regular_file;
        fs_type_s = '-';
    }
}

/**
 * Save permission in string format in class defined
 * variable fs_permissions_s.
 * Permissions are in standard UNIX/Linux readable format
 * Like rwx-wx-w-
 * @param p - std::filesystem::perms
 */
void Entry::save_permissions(fs::perms p) {
    fs_permissions_s += (p & fs::perms::owner_read) != fs::perms::none ? "r" : "-";
    fs_permissions_s += (p & fs::perms::owner_write) != fs::perms::none ? "w" : "-";
    fs_permissions_s += (p & fs::perms::owner_exec) != fs::perms::none ? "x" : "-";

    fs_permissions_s += (p & fs::perms::group_read) != fs::perms::none ? "r" : "-";
    fs_permissions_s += (p & fs::perms::group_write) != fs::perms::none ? "w" : "-";
    fs_permissions_s += (p & fs::perms::group_exec) != fs::perms::none ? "x" : "-";

    fs_permissions_s += (p & fs::perms::others_read) != fs::perms::none ? "r" : "-";
    fs_permissions_s += (p & fs::perms::others_write) != fs::perms::none ? "w" : "-";
    fs_permissions_s += (p & fs::perms::others_exec) != fs::perms::none ? "x" : "-";
}

