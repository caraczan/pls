//
// Created by karaczan on 11.01.23.
//

#include "pls_options.hpp"

// === CONSTRUCTORS ===


// === FUNCTIONS ===
/**
 * [Options::parse_program_config description]
 * Function returns true or false whether it was able to parse configuration file.
 *
 * F Uses toml++ library in order to read from toml file and assign values to correct
 * flags in Arguments instance. 
 * 
 * @return bool
 */
bool Options::parse_program_config() {

    std::string target_path = getenv("HOME");
    target_path.append("/.config/pls");
    fs::create_directories(target_path);
    target_path.append("/config.toml");

    try {
        toml::table tbl = toml::parse_file(target_path);
        // std::cout << tbl["options"]["configuration"] << std::endl;

        auto fl = tbl["options"]["configuration"]["list"].as_boolean()->get();
        // std::cout << "fl " << fl << " !!fl " << !!fl << " fl.value " << fl.is_boolean() << std::endl;
        args->arguments["flag_list"] = fl;
        
        auto ft = tbl["options"]["configuration"]["tree"].as_boolean()->get();
        // std::cout << "ft " << ft << " !!ft " << !!ft << " ft.value " << ft.value_or("") << std::endl;
        args->arguments["flag_tree"] = ft;

        auto fa = tbl["options"]["configuration"]["all"].as_boolean()->get();
        // std::cout << "fa " << fa << " !!fa " << !!fa << std::endl;
        args->arguments["flag_all"] = fa;
    } catch (const toml::parse_error& err) {
        // std::cerr << "Parsing failed:\n" << err << "\n";
        // return false;
    }

    return true;
}

/**
 * Function that parses parameters, argc and argv out of defined in this function options
 * we want.
 * @return - returns true or false if there is error.
 */
bool Options::parse() {
    // Creating options and messages for --help and for program to understand
    // what flags we want.
    po::options_description general("General options");
    general.add_options()
        ("help,h", "Prints help message")
        ("version,v", "Prints program version")
        ;

    po::options_description config("Configuration options");
    config.add_options()
        ("all,a", "List all files and directories")
        ("list,l", "Print files and directories in list format")
        ;

    po::options_description hidden("Hidden options");
    hidden.add_options()
        ("target-fs_path", po::value<std::string>()->default_value("."), "Target fs_path")
        ;

    // adding those flags created above to one option description
    po::options_description all_options;
    all_options.add(general);
    all_options.add(config);
    all_options.add(hidden);

    // positional argument that holds path where we will be listing
    po::positional_options_description p;
    p.add("target-fs_path", -1);

    // creating variables map with passed arguments
    po::variables_map opts;
    po::store(po::command_line_parser(argc, argv).
                options(all_options).
                positional(p).
                run(),
            opts
            );

    // General
    // Prints correct message and exits program with 0
    if(opts.count("help") or opts.count("h")) {
        std::cout << general << config << std::endl;
        exit(0);
    }
    if (opts.count("version") or opts.count("v")) {
        std::cout << CMAKE_NAME << " v-" << CMAKE_VERSION << "\n";
        exit(0);
    }

    // Config
    // Saves argument to std::map correct flag_
    if (opts.count("all") or opts.count("a")) {
        args->arguments["flag_all"] = true;
    }
    if (opts.count("list") or opts.count("l")) {
        args->arguments["flag_list"] = true;
    }

    // Positional/Hidden
    // Hidden from message argument, that holds path to where we want to list files
    if (opts.count("target-fs_path")) {
        args->target_path = opts["target-fs_path"].as<std::string>();
    }

    po::notify(opts);

    return true;
}

//private:
//    // Boost doesn't offer any obvious way to construct a usage string
//    // from an infinite list of positional parameters.  This hack
//    // should work in most reasonable cases.
//    std::vector<std::string> get_unlimited_positional_args_(const po::positional_options_description& p)
//    {
//        assert(p.max_total_count() == std::numeric_limits<unsigned>::max());
//
//        std::vector<std::string> parts;
//
//        // reasonable upper limit for number of positional options:
//        const int MAX = 1000;
//        std::string last = p.name_for_position(MAX);
//
//        for(size_t i = 0; true; ++i)
//        {
//            std::string cur = p.name_for_position(i);
//            if(cur == last)
//            {
//                parts.push_back(cur);
//                parts.push_back('[' + cur + ']');
//                parts.push_back("...");
//                return parts;
//            }
//            parts.push_back(cur);
//        }
//        return parts; // never get here
//    }
//
//    std::string make_usage_string_(const std::string& program_name, const po::options_description& desc, po::positional_options_description& p)
//    {
//        std::vector<std::string> parts;
//        parts.push_back("Usage: ");
//        parts.push_back(program_name);
//        size_t N = p.max_total_count();
//        if(N == std::numeric_limits<unsigned>::max())
//        {
//            std::vector<std::string> args = get_unlimited_positional_args_(p);
//            parts.insert(parts.end(), args.begin(), args.end());
//        }
//        else
//        {
//            for(size_t i = 0; i < N; ++i)
//            {
//                parts.push_back(p.name_for_position(i));
//            }
//        }
//        if(desc.options().fs_size_byte() > 0)
//        {
//            parts.push_back("[options]");
//        }
//        std::ostringstream oss;
//        std::copy(
//                parts.begin(),
//                parts.end(),
//                std::ostream_iterator<std::string>(oss, " "));
//        oss << '\n' << desc;
//        return oss.str();
//    }