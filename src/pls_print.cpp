//
// Created by karaczan on 12.01.23.
//
#include "pls_print.hpp"
// ---=== public ===---
/**
 * Chooses which printing function should be choosen based on given flag
 */
void Print::print() {
    if (args->arguments["flag_list"]) {
      // List like printing, of entries, one after another
      print_list();
    } else if (args->arguments["flag_tree"]) {
      // tree like printing of directory structure
      // TODO add this functionality
      std::cerr << "!!! Not implemented !!!" << std::endl;
      exit(-2);
    } else {
      // Standard printing, entries side by side
      print_std();
    }
}

/**
 * Prints entries in standard, side by side format
 */
void Print::print_std() {
    for(const auto& entry : entries_list->entries) {
        // chooses what color text should be, based on type of the file
        std::string_view _color = choose_color(entry);
        // prints filename
        std::cout << _color << entry.fs_name << colors.reset << " ";
    }
    // std::endl clears buffor, this action is lengthy and costly
    // it's better to use '\n' as it may yield better results with large number of listings
    std::cout << '\n';
}

/**
 * Prints entries in list format
 */
void Print::print_list() {
    using namespace std::chrono_literals;

    // Printing header for listing, it's to make it easier for people who are new and don't know what certain values means
    std::cout << colors.boldgreen
             << entries_list->headers[0] << " "
             << std::setw(entries_list->col_widths["group"])    << entries_list->headers[1] << " "
             << std::setw(entries_list->col_widths["owner"])    << entries_list->headers[2] << " "
             << std::setw(entries_list->col_widths["size"] + 2) << entries_list->headers[3] << " "
             << std::setw(entries_list->col_widths["time"])     << entries_list->headers[4] << " " << std::setw(0)
             << entries_list->headers[5] << " "
             << colors.reset
             << '\n';

    for(const auto& entry : entries_list->entries) {
        // chooses what color text should be, based on type of the file
        std::string_view _color = choose_color(entry);

        // printing file type, permissions, size, group and owner, and name.
        std::cout << entry.fs_type_s  << entry.fs_permissions_s << " "
                  << std::setw(entries_list->col_widths["group"]) << entry.fs_group   << " "
                  << std::setw(entries_list->col_widths["owner"]) << entry.fs_owner   << " "
                  << std::setw(entries_list->col_widths["size"])  << entry.fs_size_s  << entry.fs_size_unit << " "
                  << std::setw(entries_list->col_widths["time"])  << entry.fs_last_write_time_s << " ";
                  // << _color << entry.fs_name << colors.reset;

        if (entry.fs_is_symlink)
            std::cout << colors.cl_symlink << entry.fs_name << colors.reset
                      << " -> " << _color << entry.fs_symlink_target_path << colors.reset << " ";
        else
            std::cout << _color << entry.fs_name << colors.reset;

        std::cout << '\n';
        // std::endl clears buffor, this action is lengthy and costly
        // it's better to use '\n' as it may yield better results with large number of listings
    }
}

// ---=== private ===---
/**
 * Chooses color based on type of entry given to it in parameter
 * @param entry
 * @return std::string_view, color cast from defines
 */
std::string_view Print::choose_color(const Entry &entry) {
    // color that will be returned
    std::string_view _color {};

    // choosing color based on entry.fs_type which is enum instance
    switch (entry.fs_type) {
        case Entry::fs_typer::is_regular_file:
            _color = colors.cl_regular_file;
            if (entry.fs_is_executable) {
                _color = colors.cl_exe_file;
            }
            break;
        case Entry::fs_typer::is_directory:
            _color = colors.cl_dir;
            break;
        case Entry::fs_typer::is_symlink:
            // _color = colors.cl_symlink;
            break;
        case Entry::fs_typer::is_block_file:
        case Entry::fs_typer::is_character_file:
            _color = colors.cl_special;
            break;
        case Entry::fs_typer::is_fifo:
            _color = colors.cl_fifo;
            break;
        case Entry::fs_typer::is_socket:
            _color = colors.cl_socket;
            break;
    }

    return _color;
}
