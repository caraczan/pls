/**
 * Created by caraczan 25.01.2023
 */

#ifndef PLS_OPTIONS_HPP
#define PLS_OPTIONS_HPP

// If name and version from cmake(via compiler flag -D) are not passed
// assigning default strings for preprocessor directives.
#ifndef CMAKE_NAME
#define CMAKE_NAME "pls"
#endif

#ifndef CMAKE_VERSION
#define CMAKE_VERSION "?"
#endif

// Includes
#include <boost/program_options.hpp>
#include <filesystem>
#include <iterator>
#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>
// Include tomlplusplus as header only from submodule
// for library to parse toml config files
#include <toml++/toml.h>

// namespace alias
namespace po = boost::program_options;
namespace fs = std::filesystem;

struct Arguments {
    std::string target_path {"."};
    std::map<std::string, bool> arguments {
        {"flag_all",  false},
        {"flag_list", false},
        {"flat_tree", false}
    };
};

// Classes

/**
 * Wrapper around boost::program_options handling all flags
 * and saving output to std::map flags and std::string target_path
 * for use in List, Entry and Print.
 */
class Options {
private:
  int argc;
  char **argv;
  Arguments *args;
public:
    // === VARIABLES ===

    // === FUNCTIONS ===
    bool parse();
    bool parse_program_config();

    // === CONSTRUCTORS ===
    Options(int _argc, char** _argv, Arguments *_args) :
        argc(_argc), argv(_argv), args(_args)
    {};
};

#endif // PLS_OPTIONS_HPP
