//
// Created by karaczan on 25.01.23.
//

#ifndef PLS_PLS_ENTRY_HPP
#define PLS_PLS_ENTRY_HPP

// Includes
#include <filesystem>
#include <cmath>
#include <sstream>

// from C
#include <pwd.h>
#include <grp.h>
#include <sys/stat.h>

// namespace aliases
namespace fs = std::filesystem;

// Classes
/**
 * Storage for data about one file(any linux possible type).
 */
class Entry {
private:
    // === FUNCTIONS ===
    void save_permissions(fs::perms p);
    void assign_type(const fs::directory_entry& fs_entry);
    void convert_to_unit();
    void determine_user_and_gruop();
    double round_up(double value, int decimal_places);
public:
    // === VARIABLES ===
    // enum describing possible types of file on linux/mac
    enum fs_typer : char32_t {
        is_regular_file     = '-',
        is_directory        = 'd',
        is_symlink          = 'l',
        is_block_file       = 'b',
        is_character_file   = 'c',
        is_fifo             = 'p',
        is_socket           = 's'
    };
    // enum instance which holds correctly assigned file type from enum
    Entry::fs_typer fs_type {};
    // char representation of file type, for use in Print
    std::string fs_type_s {'-'};

    std::string fs_owner {};
    std::string fs_group {};

    // file name
    std::string fs_name {};

    // file size in bytes
    uint64_t fs_size_byte {0};
    double fs_size {0};
    std::string fs_size_s {"0"};
    std::string fs_size_unit {"By"};
    
    // file datetime
    fs::file_time_type fs_last_write_time {};
    std::string fs_last_write_time_s {""};
    // file permissions in string representation
    std::string fs_permissions_s {};
    // whether file is executable
    bool fs_is_executable {false};

    // symlink specific fields
    bool fs_is_symlink {false};
    // path to symlink target
    std::string fs_symlink_target_path {};


    // === FUNCTIONS ===
    bool operator < (const Entry& obj) const;
    bool operator > (const Entry& obj) const;

    // === CONSTRUCTORS ===
    explicit Entry(const fs::directory_entry& fs_entry);
};

#endif
