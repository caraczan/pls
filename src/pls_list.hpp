//
// Created by karaczan on 25.01.23.
//

#ifndef PLS_PLS_LIST_HPP
#define PLS_PLS_LIST_HPP

// Includes
#include <filesystem>
#include <cstdint>
#include <ostream>
#include <vector>
#include <map>
#include <algorithm>

// Internal Includes
#include "pls_entry.hpp"
#include "pls_options.hpp"

/**
 * List of objects of class Entry
 *
 * Implements necessary filtering of list based on passed arguments.
 */
class List {
private:
    // === VARIABLES ===
    Arguments *args;
    // === FUNCTIONS ===
public:
    // === VARIABLES ===
    std::vector<Entry> entries {};
    std::vector<std::string> headers {
        {"Permission"},
        {"Group"},
        {"Owner"},
        {"Size"},
        {"Time"},
        {"Name"},
    };
    std::map<std::string, uint64_t> col_widths {
        {"perm", 0},
        {"group", 0},
        {"owner", 0},
        {"size", 0},
        {"time", 0}
    };

    // === FUNCTIONS ===
    void filter();
    void sort();
    void calcualate_col_widths();

    // === CONSTRUCTORS ===
    List(Arguments *_args) :
        args(_args)
    {
        std::string tp = args->target_path;
        auto n = std::distance(fs::directory_iterator(tp), fs::directory_iterator{});
        entries.reserve(n);

        for (const fs::directory_entry& file : fs::directory_iterator(tp)) {
            // insert file to vector, it will automatically call constructor of Entry class
            entries.emplace_back(file);
        }
    };
};

#endif