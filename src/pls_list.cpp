//
// Created by karaczan on 12.01.23.
//

#include "pls_list.hpp"

// ---=== public ===---
void List::filter() {
    for (size_t i = 0; i < entries.size(); i++) {
      // if we want dont want to see `hidden` files
        if (!args->arguments["flag_all"] and entries[i].fs_name[0] == '.') {
            entries.erase(std::next(entries.begin(), i));
            i--;
        }
    }
}

/**
 * For now sorts based on name
 */
void List::sort() {
    std::sort(entries.begin(), entries.end());
}

void List::calcualate_col_widths() {
    uint64_t max_w_perm  {headers[0].length()};
    uint64_t max_w_group {headers[1].length()};
    uint64_t max_w_owner {headers[2].length()};
    uint64_t max_w_size  {headers[3].length()};
    uint64_t max_w_time  {headers[4].length()};

    for (auto entry : entries) {
        // std::cout << " " << entry.fs_permissions_s << " " << entry.fs_permissions_s.length() << " \n";
        if (entry.fs_permissions_s.length() > max_w_perm)
            max_w_perm = entry.fs_permissions_s.length() + entry.fs_type_s.length();

        if (entry.fs_group.length() > max_w_group)
            max_w_group = entry.fs_group.length();

        if (entry.fs_owner.length() > max_w_owner)
            max_w_owner = entry.fs_owner.length();

        if (entry.fs_size_s.length() > max_w_size)
            max_w_size = entry.fs_size_s.length();

        if (entry.fs_last_write_time_s.length() > max_w_size)
            max_w_time = entry.fs_last_write_time_s.length();
    }

    col_widths["perm"]  = max_w_perm;
    col_widths["group"] = max_w_group;
    col_widths["owner"] = max_w_owner;
    col_widths["size"]  = max_w_size;
    col_widths["time"]  = max_w_time;
}


// ---=== private ===---

