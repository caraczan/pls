#!/usr/bin/python3

"""
Install script for this 'pls' program
"""

import os
import shutil
import pathlib


class DV:
    """
    DV - DefaultValues is class holding all default values
    for easier changing in the future.
    """
    HOME_PATH: str = pathlib.Path().home()

    # P is abbreviation from PROGRAM

    P_PATH: str = "./build/pls"
    P_TARGET_PATH: str = "/usr/bin/pls"

    P_CONFIG_PATH: str = "config/pls/config.toml"
    P_TARGET_CONFIG_PATH: str = f"{HOME_PATH}/.config/pls"


def install_program() -> bool:
    """
    Copies program binary from P_PATH to P_TARGET_PATH.

    Returns True or False for either success or failure.
    """
    print("Attemting at copying program to /usr/bin/.")
    print("This action requires your elevated privalages.")
    print("After this program will be availaible from anywhere.")

    try:
        os.system(f"sudo cp {DV.P_PATH} {DV.P_TARGET_PATH}")
    except os.OSError:
        raise os.OSError
        return False

    return True

    print("Installation succesfull")


def copy_config() -> bool:
    """
    Copies config file from P_CONFIG_PATH to P_TARGET_CONFIG_PATH.

    If directory doesn't exists it will be created.

    Returns True or False for either success or failure.
    """
    print(f"Attemting at copying config file ({DV.P_CONFIG_PATH}) to '{DV.P_TARGET_CONFIG_PATH}'")

    try:
        pathlib.Path(DV.P_TARGET_CONFIG_PATH).mkdir(parents=True, exist_ok=True)
        shutil.copy(DV.P_CONFIG_PATH, f"{DV.P_TARGET_CONFIG_PATH}/config.toml")
    except os.OSError:
        raise os.OSError
        return False

    print("Copying succesfull.")

    return True


def main() -> bool:
    try:
        install_program()
    except OSError:
        raise OSError
        return False

    try:
        copy_config()
    except OSError:
        raise OSError
        return False

    return True


if __name__ == '__main__':
    exit(main())
