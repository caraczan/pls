# pls - please list

[![pipeline status](https://gitlab.com/caraczan/pls/badges/master/pipeline.svg)](https://gitlab.com/caraczan/pls/commits/master)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

---

Simple file listing program with nice defaults,

and ability to set options in `$HOME/.config/config.toml`.

---

## Installation

### cloning

To clone project use below command.

`git clone --recurse-submodules -j8 https://gitlab.com/caraczan/pls.git`

This will clone project with submodules.

### Required software for compilation

- `g++` or `clang` supporting C++17
- `Cmake` min v3.18
- `ninja`
- `Boost libraries` minumum v1.74.0 (require installing from package manager)
- `tomlplusplus` minimum v3.3.0 (included as submodule of the project)
- `python` minimum v3.8 (optional, for use on project building scripts)

#### Boost packages

Plese search and install proper Boost lib packages for your own distributions. You don't need to install everything, just `program options`.

##### for openSUSE Tumpleweed (rolling release, possibly Leap)

- `libboost_program_options1_80_0`
- `libboost_program_options-devel`

##### for debian bulseye (possibly debian-based)

- `libboost-dev` (you might also need to install build-essential)
- `libboost-program-options-dev` (1.74.0 is latest stable on debian)


### Instructions

`construct_project.py` or `construct_project.sh` will generate project files and compile files.

This will also update clangd-lsp database with project information from cmake, if `compdb` program is available. 
For more information/options checkout scripts, with `-h` flag and by reading it.

```bash
python3 construct_project.py -c
```

or

```bash
bash construct_project.py -c
```

or manually 

```bash
# creating directory where all temporary and end files will be stored
mkdir build
cd build
# generating build files for Ninja
cmake -G Ninja ..
cd ..
# compiling and linking project with 6 threads and name `pls`
cmake --build ./build --target pls -j 6
```

Next install program to directory that is in the `$PATH`, like `/bin`, `/usr/bin`, `$HOME/.local/bin`...

I've prepared script to automate this `install.py`. It will copy program binary `./build/pls` to `/usr/bin/pls`.

Additionally it will copy configuration file `./config/pls/config.toml` to `$HOME/.config/pls/config.toml`.

### Configuration

Program have optional configuration file.

config file is in `config/pls/config.toml`, whith all available options written.

To make use of this copy it to `$HOME/.config/pls/` directory.

`cp -r config/pls $HOME/.config/`

## Usage possibilities

```bash
./pls 
./pls -a # or `--all` 
./pls -l # or `--list` 
./pls -la # or `-l -a` or `--list --all`
./pls "../" # relative path
./pls "/home/" # absolute path
...
```

## Credits and sources

- [Boost](https://www.boost.org/) licensed unser __Boost Software License__
- [tomlplusplus](https://github.com/marzer/tomlplusplus)  licensed under __MIT__
- [Monochrome-Sauce](https://github.com/Monochrome-Sauce) for help with automation and clang


## TODO
- [x] ~~include Boost lib with project for static linking and compiling~~ use system Boost library
- [x] colors to output based on type of file
  - [x] type cheking and color coding
- [ ] gitlab ci/cd
  - [x] add build ci/cd
  - [ ] add tests to ci/cd
- [x] properly structure program into classes and files
- [x] `-l or --list` have to return all information
  - [x] file type
  - [x] file permissions
  - [x] file group alignment
  - [x] file owner
  - [x] file last edit date
  - [x] file size
  - [x] file name
  - [x] symlink handling
- [ ] `-t or --tree` to implement tree type listing
- [x] add reading options from places like `$HOME/.config/pls/config.toml`
- [x] ~~mini script to copy config files into place~~ Script that copies program and config file into proper places.
