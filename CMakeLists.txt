cmake_minimum_required(VERSION 3.12)
# C++ version
set(CMAKE_CXX_STANDARD 17)
# project information
project(pls
            VERSION 0.3.2
            DESCRIPTION "Yet another program listing files and directories"
            HOMEPAGE_URL "https://gitlab.com/caraczan/pls/"
            LANGUAGES CXX
        )

# passing variables to preprocessor, via compiler -D flag 
add_definitions(-DCMAKE_NAME="${CMAKE_PROJECT_NAME}")
add_definitions(-DCMAKE_VERSION="${CMAKE_PROJECT_VERSION}")
add_definitions(-DCMAKE_HOMEPAGE_URL="${CMAKE_PROJECT_HOMEPAGE_URL}")

# adding source and header files
set(SOURCE_FILES
        src/main.cpp
        src/pls_options.cpp
        src/pls_print.cpp
        src/pls_list.cpp
        src/pls_entry.cpp
    )
set(HEADER_FILES
        src/pls_options.hpp
        src/pls_print.hpp
        src/pls_list.hpp
        src/pls_entry.hpp
    )

# Compilation option
set(CMAKE_BUILD_TYPE DEBUG)
set(CMAKE_CXX_FLAGS_DEBUG "-g -Wall -Werror -Wall -Wextra -pedantic")
set(CMAKE_CXX_FLAGS_RELEASE "-O2")

# Boost libs configuration
# set(Boost_USE_STATIC_LIBS ON)
# set(Boost_USE_RELEASE_LIBS OFF)

# set if you compiled your own Boost library and put it somewhere else
# set(BOOST_ROOT ${CMAKE_SOURCE_DIR}/libraries/boost)
# set(Boost_INCLUDE_DIR ${BOOST_ROOT}/1.80.0/include)
# set(Boost_LIBRARY_DIR ${BOOST_ROOT}/1.80.0/lib)
find_package(Boost 1.74.0 COMPONENTS program_options REQUIRED)
include_directories(${Boost_INCLUDE_DIR} "tomlplusplus/include")

if(Boost_FOUND)
    add_executable(pls ${SOURCE_FILES})
    target_link_libraries(pls ${Boost_LIBRARIES})
endif()